package net.sf.okapi.connectors.microsoft;

import java.util.List;

public interface MicrosoftMTAPI {

    /**
     * Call the getTranslations() API method.
     * @return API responses, or null if the API call fails
     */
	List<TranslationResponse> getTranslations(String query, String srcLang, String trgLang, int maxHits, int threshold);

    /**
     * Call the getTranslationsArray() API method.
     * @return API responses, or null if the API call fails
     */
	List<List<TranslationResponse>> getTranslationsArray(GetTranslationsArrayRequest request, String srcLang,
			String trgLang, int maxHits, int threshold);
}